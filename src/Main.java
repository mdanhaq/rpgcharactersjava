import character.*;
import Items.*;
import HandlingExceptions.*;

public class Main {

    public static void main(String[] args) {
        Warrior warriorOne = new Warrior("warrior");
        warriorOne.showStats();
        warriorOne.levelUp();
        warriorOne.showStats();

        try{
            warriorOne.equipArmor(new Armor(Slot.BODY, 1, "Body" , ArmorType.PLATE, 2,2,2,2));
        }catch(InvalidArmorException ex){
            System.out.println("There occurs exception: " + ex.getMessage());
        }
        warriorOne.showStats();

        try{
            warriorOne.unequip(Slot.BODY);
        }catch(NoArmorEquippedException ex){
            System.out.println("There occurs exception: "+ex.getMessage());
        }
        warriorOne.showStats();

        try{
            warriorOne.equipWeapon(new Weapon(Slot.WEAPON, 1, "Axe", WeaponType.AXE, 3,2));
        }catch(InvalidWeaponException ex){
            System.out.println("There occurs exception: "+ex.getMessage());
        }
        try{
            warriorOne.unequip(Slot.WEAPON);
        }catch(NoArmorEquippedException ex){
            System.out.println("There occurs exception: "+ex.getMessage());
        }
        warriorOne.showStats();
    }
}
