package Attributes;

public class PrimaryAttribute  {
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public PrimaryAttribute(){
        this.strength = 0;
        this.dexterity = 0;
        this.intelligence = 0;
        this.vitality = 0;
    }
    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getStrength() {
        return this.strength;
    }

    public int getDexterity() {
        return this.dexterity;
    }

    public int getIntelligence() {
        return this.intelligence;
    }

    public int getVitality() {
        return this.vitality;
    }

    /**
     * Returns the total primary attributes of a character by using them as parameter
     */
    public int getCharacterTotalVitality(int baseStatsVitality) {
        return this.vitality + baseStatsVitality;
    }

    public int getCharacterTotalIntelligence(int baseStatsIntelligence) {
        return this.intelligence + baseStatsIntelligence;
    }
    public int getCharacterTotalDexterity(int baseStatsDexterity) {
        return this.dexterity + baseStatsDexterity;
    }

    public int getCharacterTotalStrength(int baseStatsStrength) {
        return this.strength + baseStatsStrength;
    }

    /**
     * Updating primary attributes according to
     * @param strength
     */

    public void updateStrength(int strength) {
        this.strength += strength;
    }

    public void updateIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }

    public void updateDexterity(int dexterity) {
        this.dexterity += dexterity;
    }

    public void updateVitality(int vitality) {
        this.vitality += vitality;
    }

    /**
     * Recalculating primary attributes when unequipping armor
     */
    public void resetPrimaryAttributes(){
        this.vitality = 0;
        this.dexterity = 0;
        this.intelligence = 0;
        this.strength = 0;
    }
}
