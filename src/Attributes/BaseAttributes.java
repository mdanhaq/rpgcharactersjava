package Attributes;

public class BaseAttributes {

    private int strength;
    private int dexterity;
    private int vitality;
    private int intelligence;

    public BaseAttributes() {
        this.strength = 0;
        this.dexterity = 0;
        this.vitality = 0;
        this.intelligence = 0;
    }

    /**
     * Stats or base attributes of Characters at level 1.
     */

    public void setMageStats(){
        this.strength = 1;
        this.dexterity = 1;
        this.vitality = 5;
        this.intelligence = 8;
    }

    public void setWarriorStats(){
        this.strength = 5;
        this.dexterity = 2;
        this.vitality = 10;
        this.intelligence = 1;
    }

    public void setRogueStats(){
        this.strength = 2;
        this.dexterity = 6;
        this.vitality = 8;
        this.intelligence = 1;
    }

    public void setRangerStats(){
        this.strength = 1;
        this.dexterity = 7;
        this.vitality = 8;
        this.intelligence = 1;
    }

    public int getDexterity(){
        return this.dexterity;
    }
    public int getVitality(){
        return this.vitality;
    }
    public int getStrength(){
        return this.strength;
    }
    public int getIntelligence(){
        return this.intelligence;
    }

    /**
     * Increasing attributes as the characters level is up
    */

    public void updateDexterity(int dexterity){
        this.dexterity += dexterity;
    }
    public void updateVitality(int vitality){
        this.vitality += vitality;
    }
    public void updateStrength(int strength){
        this.strength += strength;
    }
    public void updateIntelligence(int intelligence){
        this.intelligence += intelligence;
    }
}
