package Items;

/**
 * Types of existing Armor
 */
public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
