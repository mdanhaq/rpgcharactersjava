package Items;

/**
 * All items have names, required level to equip the item and
 * slot in which the item is equipped
 */
public abstract class Item {
    private Slot slot;
    private int reqLevel;
    private String name;

    public Item(Slot slot, int reqLevel, String name){
        this.slot = slot;
        this.reqLevel = reqLevel;
        this.name = name;
    }
    public Slot getSlot(){
        return this.slot;
    }
    public int getReqLevel(){
        return this.reqLevel;
    }
}
