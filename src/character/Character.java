package character;

import Items.*;
import HandlingExceptions.*;
import Attributes.*;

import java.util.HashMap;

public abstract class Character {
    protected String name;
    protected int level;
    protected RoleClass roleClass;
    protected BaseAttributes baseAttributes;
    protected PrimaryAttribute primaryAttribute;
    protected SecondaryAttribute secondaryAttribute;
    protected HashMap<Slot, Item> equipped;
    protected double dps;

    /**
     * Character constructor that sets the shared attributes.
     */
    public Character(String name) {
        this.name = name;
        this.level = 1;
        this.baseAttributes = new BaseAttributes();
        this.primaryAttribute = new PrimaryAttribute();
        this.secondaryAttribute = new SecondaryAttribute();
        this.equipped = new HashMap<Slot, Item>();

    }

    public int getLevel() {
        return this.level;
    }

    public double getDps() {
        return this.dps;
    }

    /**
     * Show the stats/attributes of the character
     */
    public void showStats() {
        System.out.println("Name:\t" + this.name);
        System.out.println("Class:\t" + this.roleClass);
        System.out.println("Level:\t" + this.level);
        System.out.println("Primary Attirbutes:\tStrength: " +
                this.primaryAttribute.getCharacterTotalStrength(this.baseAttributes.getStrength()) + ", Dexterity: " +
                this.primaryAttribute.getCharacterTotalDexterity(this.baseAttributes.getDexterity()) + ", Intelligence: " +
                this.primaryAttribute.getCharacterTotalIntelligence(this.baseAttributes.getIntelligence()) + ", Vitality: " +
                this.primaryAttribute.getCharacterTotalVitality(this.baseAttributes.getVitality()));
        System.out.println("Secondary Attributes:\tHealth: " + this.secondaryAttribute.getHealth() + ", Armor Rating: " +
                this.secondaryAttribute.getArmorRating() + ", Elemental Resistance: " +
                this.secondaryAttribute.getElementalResistance());
        System.out.println("DPS:\t" + this.dps + "\n");

    }

    public void updatePrimaryAndSecondaryStats() {
        this.primaryAttribute.resetPrimaryAttributes();
        for (HashMap.Entry<Slot, Item> entry : this.equipped.entrySet()) {
            if (entry.getKey() != Slot.WEAPON) {
                this.primaryAttribute.updateStrength(((Armor) entry.getValue()).getStrength());
                this.primaryAttribute.updateDexterity(((Armor) entry.getValue()).getDexterity());
                this.primaryAttribute.updateIntelligence(((Armor) entry.getValue()).getIntelligence());
                this.primaryAttribute.updateVitality(((Armor) entry.getValue()).getVitality());
                this.secondaryAttribute.updateElementalResistance(((Armor) entry.getValue()).getIntelligence());
                this.secondaryAttribute.updateArmorRation(((Armor) entry.getValue()).getDexterity(), ((Armor) entry.getValue()).getDexterity());
                this.secondaryAttribute.updateHealth(((Armor) entry.getValue()).getVitality());
            }
        }
    }

    /**
     * Removes the equipment with a certain key(slot). Throws an exception if the hashmap don't have that key.
     * @param slot - Slot enum.
     * @throws NoArmorEquippedException
     */
    public void unequip(Slot slot) throws NoArmorEquippedException {

        if (!this.equipped.containsKey(slot)) {
            throw new NoArmorEquippedException("Do not have anything equipped at that slot.");
        } else if (((Item) this.equipped.get(slot)).getSlot() != Slot.WEAPON) {
            this.secondaryAttribute.updateArmorRation(-((Armor) this.equipped.get(slot)).getStrength(), ((Armor) this.equipped.get(slot)).getDexterity());
            this.secondaryAttribute.updateHealth(-((Armor) this.equipped.get(slot)).getVitality());
            this.secondaryAttribute.updateElementalResistance(-((Armor) this.equipped.get(slot)).getIntelligence());
        }
        this.equipped.remove(slot);
        updatePrimaryAndSecondaryStats();
        calculateDps();
    }


    abstract public void equipWeapon(Weapon weapon) throws InvalidWeaponException;

    abstract public void equipArmor(Armor armor) throws InvalidArmorException;

    abstract public void calculateDps();

    abstract public void levelUp();

    abstract public void levelUp(int levels) throws IllegalArgumentException;

}
