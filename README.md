## RPG Character
RPG Characters (Heroes) is a console application resembling the features and relationships typical of a role playing game. It is designed with a view of extendability and scalability. Various OOP concepts such as SOLID principles and design patterns are applied.

## Overview
There are four characters - Warriors, Rangers, Rogues and Mages. Each has attributes Health, Strength, Dexterity and Intelligence. They can equip themselves with Weapons (Melee, Range or Magic) or Armor (Cloth, Leather or Plate), where the latter can be placed on different body parts - Head, Body or Legs.

Both characters and equipment have levels that influence various attributes. In addition, different character attributes may influence the effect of any equipment item they hold, whereas the placement of armor also determines it's protection efficacy.

## Technologies
IntelliJ with JDK 16

### Key Takeways
Using the pillars of OOP Cocepts (Abstraction, Encapsulation, Inheritance, Polymorphism) 
Unit testing is an integral part of any well-designed system. 
